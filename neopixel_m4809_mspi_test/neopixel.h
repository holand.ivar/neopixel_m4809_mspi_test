/*
 * neopixel.h
 *
 * Created: 01.09.2018 00:41:28
 * Author: Terje
 */


#ifndef NEOPIXEL_H_
#define NEOPIXEL_H_

#include <avr/io.h>

#define F_CPU 10000000UL  // 10MHz
#define RGB_LEDSTRING_LENGTH    (20)

//#define ALTERNATE_PINS
//#define VISIBLE_SCK_PIN

#ifdef ALTERNATE_PINS
#define LED_USART           USART0
#define LED_USART_PORTMUX()	(PORTMUX.CTRLB |= PORTMUX_USART0_ALTERNATE_gc)
#define LED_DATA_PORT       PORTA
#define LED_DATA_PIN        PIN1_bm
#define LED_DATA_PINCTRL    PIN1CTRL
#define LED_SCK_PORT        PORTA
#define LED_SCK_PIN         PIN3_bm
#else
#define LED_USART           USART0
//#define LED_USART_PORTMUX()	(PORTMUX.CTRLB &= ~PORTMUX_USART0_ALTERNATE_gc)
#define LED_DATA_PORT       PORTA
#define LED_DATA_PIN        PIN0_bm
#define LED_DATA_PINCTRL    PIN0CTRL
#define LED_SCK_PORT        PORTA
#define LED_SCK_PIN         PIN2_bm
#endif

/*
 * The structs you need
 */

typedef __uint24 uint24_t;

typedef union {
	struct {
		uint8_t b;
		uint8_t r;
		uint8_t g;
	};
	uint24_t channel;
} color_t;

typedef struct {
	uint16_t h;
	uint8_t s;
	uint8_t b;
} hsb_t;

/*
 * Defines that might be interesting to use
 */

#define GREEN   (0x800000)
#define RED     (0x008000)
#define BLUE    (0x000080)
#define YELLOW  (0x808000)
#define MAGENTA (0x008080)
#define CYAN    (0x800080)
#define WHITE   (0x808008)
#define BLACK   (0x000000)
#define ORANGE  (0x408000)
#define INDIGO  (0x004080)
#define VIOLET  (0x00486A)

#define GREEN_RGBW   (0x80000000)
#define RED_RGBW     (0x00800000)
#define BLUE_RGBW    (0x00008000)
#define YELLOW_RGBW  (0x80800000)
#define MAGENTA_RGBW (0x00808000)
#define CYAN_RGBW    (0x80008000)
#define WHITE_RGBW   (0x00000080)
#define BLACK_RGBW   (0x00000000)
#define ORANGE_RGBW  (0x40800000)
#define INDIGO_RGBW  (0x00408000)
#define VIOLET_RGBW  (0x00486A00)

#define GREEN_RGBW_100    (0x80000000)
#define GREEN_RGBW_75     (0x60000000)
#define GREEN_RGBW_50     (0x40000000)
#define GREEN_RGBW_25     (0x20000000)
#define GREEN_RGBW_15     (0x10000000)
#define GREEN_RGBW_5      (0x04000000)

#define RED_RGBW_100      (0x00800000)
#define RED_RGBW_75       (0x00600000)
#define RED_RGBW_50       (0x00400000)
#define RED_RGBW_25       (0x00200000)
#define RED_RGBW_15       (0x00100000)
#define RED_RGBW_5        (0x00040000)

#define BLUE_RGBW_100     (0x00008000)
#define BLUE_RGBW_75      (0x00006000)
#define BLUE_RGBW_50      (0x00004000)
#define BLUE_RGBW_25      (0x00002000)
#define BLUE_RGBW_15      (0x00001000)
#define BLUE_RGBW_5       (0x00000400)

#define WHITE_RGBW_100    (0x00000080)
#define WHITE_RGBW_75     (0x00000060)
#define WHITE_RGBW_50     (0x00000040)
#define WHITE_RGBW_25     (0x00000020)
#define WHITE_RGBW_15     (0x00000010)
#define WHITE_RGBW_5      (0x00000004)

#define MAGENTA_RGBW_100  (0x00808000)
#define MAGENTA_RGBW_75   (0x00606000)
#define MAGENTA_RGBW_50   (0x00404000)
#define MAGENTA_RGBW_25   (0x00202000)
#define MAGENTA_RGBW_15   (0x00101000)
#define MAGENTA_RGBW_5    (0x00040400)

#define YELLOW_RGBW_100   (0x80800000)
#define YELLOW_RGBW_75    (0x60600000)
#define YELLOW_RGBW_50    (0x40400000)
#define YELLOW_RGBW_25    (0x20200000)
#define YELLOW_RGBW_15    (0x10100000)
#define YELLOW_RGBW_5     (0x04040000)

#define CYAN_RGBW_100     (0x80008000)
#define CYAN_RGBW_75      (0x60006000)
#define CYAN_RGBW_50      (0x40004000)
#define CYAN_RGBW_25      (0x20002000)
#define CYAN_RGBW_15      (0x10001000)
#define CYAN_RGBW_5       (0x04000400)


/*
 * Private functions
 */

//static void LED_Send_Bit(uint8_t Data);

/*
 * Public functions
 */

void LED_Init(void);
void LED_Write_LED_RGB(uint24_t LED);
void LED_Write_LED_RGBW(uint32_t LED);
void LED_Write_LED_RGB_String(uint24_t *LED, uint16_t LED_string_length);
void LED_Write_LED_RGBW_String(uint32_t *LED, uint16_t LED_string_length);
void LED_Fill_LED_RGB_String(uint24_t Color, uint16_t LED_string_length);
void LED_Fill_LED_RGBW_String(uint32_t Color, uint16_t LED_string_length);
color_t hsb2rgb(hsb_t led_array);

#endif /* NEOPIXEL_H_ */