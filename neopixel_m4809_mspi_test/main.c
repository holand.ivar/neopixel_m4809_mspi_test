/*
 * main.c
 *
 * Created: 04.12.2020 16:45:00
 * Author : Ivar
 */

/*
 * Include files
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <stdlib.h>
#include "neopixel.h"

color_t hsb2rgb_v2(hsb_t led_array);
void ChristmasLights(void);
void WarmWhiteStaticLights(void);

/*
 * Global variables
 */
// The array containing the NeoPixel configuration
color_t neopixels[RGB_LEDSTRING_LENGTH] = {0};

// Variable to keep track of ring "rotation"
uint8_t move = 0;

/*
 * main()
 */

int main(void)
{
	// Configure clock
#if (F_CPU == 20000000ul) || (F_CPU == 16000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, 0);
#elif (F_CPU == 10000000ul) || (F_CPU == 8000000ul)
	_PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_PDIV_2X_gc | CLKCTRL_PEN_bm);
#endif

	// Init LEDs to black
	LED_Init();
	LED_Fill_LED_RGB_String(BLACK, RGB_LEDSTRING_LENGTH);

	// Configure RTC to wake up device after sleep
	RTC.INTCTRL = RTC_OVF_bm;
	RTC.PER = (1024 / 2); // Change this to control update interval
	RTC.CLKSEL = RTC_CLKSEL_INT1K_gc;
	RTC.CTRLA = RTC_RTCEN_bm | RTC_PRESCALER_DIV1_gc | RTC_RUNSTDBY_bm;
	set_sleep_mode(SLEEP_MODE_STANDBY);
	sei();
	sleep_mode();

	WarmWhiteStaticLights();

	while(1) {


		// Rotate LED ring with all hues up to 1535
// 		for(uint8_t i = 0; i < RGB_LEDSTRING_LENGTH; i++) {
// 			hsb_t new_setting = {
// 				.h = (1535 / RGB_LEDSTRING_LENGTH) * (i + 1),
// 				.s = 0xff, // Full saturation
// 				.b = 0x20 // Not very high brightness
// 			};
// 			neopixels[(i + move) % RGB_LEDSTRING_LENGTH] = hsb2rgb(new_setting);
// 		}
// 		if(++move >= RGB_LEDSTRING_LENGTH) {
// 			move = 0;
// 		}

		ChristmasLights();

		// Update LEDs, use atomic block to prevent interrupts during update
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
			LED_Write_LED_RGB_String((uint24_t *)neopixels, RGB_LEDSTRING_LENGTH);
		}

		// Go to sleep, wake up on RTC ISR
		sleep_mode();
	}
}

void WarmWhiteStaticLights(void)
{
	uint8_t brightness = 50;

	for (uint8_t i = 0; i < RGB_LEDSTRING_LENGTH; i = i + 3) {
		//neopixels[i] = hsb2rgb((hsb_t){.h = 180, .s = 255, .b = brightness});

		neopixels[i] = ((color_t){.r = (220*brightness) >> 8, .g = (100*brightness) >> 8, .b = (30*brightness) >> 8});

		brightness = rand() % 50 + 10;
	}
}

void ChristmasLights(void)
{
	static uint24_t wheel[4] = {RED, GREEN, BLUE, ORANGE};
	static int16_t brightness = 50;

	static uint8_t offset = 0;

	wheel[0] = hsb2rgb_v2((hsb_t){.h = 0, .s = 255, .b = brightness}).channel;
	wheel[1] = hsb2rgb_v2((hsb_t){.h = 150, .s = 255, .b = brightness}).channel;
	wheel[2] = hsb2rgb_v2((hsb_t){.h = 511, .s = 255, .b = brightness}).channel;
	wheel[3] = hsb2rgb_v2((hsb_t){.h = 1023, .s = 255, .b = brightness}).channel;

	uint8_t count = 0;
	for (uint8_t i = 0; i < RGB_LEDSTRING_LENGTH; i = i + 4) {
		neopixels[i+0].channel = wheel[(count+offset)%4];
		count++;
	}

	offset++;

	if (offset == 4) {
		offset = 0;
	}
}

color_t hsb2rgb_v2(hsb_t led_array)
{
	hsb_t input;

	input.h = led_array.h % 1535;
	input.s = led_array.s;
	input.b = led_array.b;

	return hsb2rgb(input);
}

ISR(RTC_CNT_vect)   //Just for wakeup
{
	RTC.INTFLAGS = RTC_OVF_bm;
}