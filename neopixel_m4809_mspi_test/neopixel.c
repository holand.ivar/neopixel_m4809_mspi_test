/*
 * neopixel.c
 *
 * Created: 01.09.2018 00:40:57
 * Author: Terje
 */

#include <avr/io.h>
#include <stdint.h>
#include "neopixel.h"

/*
 * Local defines
 */

#define LED_BIT_SIZE_RGB   (24)
#define LED_BIT_SIZE_RGBW  (32)
#define LED_RGB_BYTES  (LED_BIT_SIZE_RGB/8)
#define LED_RGBW_BYTES (LED_BIT_SIZE_RGBW/8)
#define LED_MASK_RGB    (1ul<<(LED_BIT_SIZE_RGB-1))
#define LED_MASK_RGBW   (1ul<<(LED_BIT_SIZE_RGBW-1))

#define LED_BAUD_CLK 625000ul
#define USART_BAUD_RATE(BAUD_RATE) ((float)((float)F_CPU * 64.0 / (16.0 * (float)BAUD_RATE)) + 0.5)
#define LED_BIT_ZERO (0x3F)
#define LED_BIT_ONE  (0x0F)

/*
 * Private functions
 */

static void LED_Send_Bit(uint8_t Data)
{
	while(!(LED_USART.STATUS & USART_DREIF_bm)) {
		// Wait for empty buffer
	}

	LED_USART.TXDATAL = Data;
}

/*
 * Public functions
 */

void LED_Init(void)
{
	//LED_USART_PORTMUX();

	LED_USART.BAUD = USART_BAUD_RATE(LED_BAUD_CLK);
	LED_USART.CTRLC = USART_CMODE_MSPI_gc;
	LED_USART.CTRLB = USART_TXEN_bm;

	// Set MOSI to output and low
	LED_DATA_PORT.LED_DATA_PINCTRL = PORT_INVEN_bm;  // Invert pin to avoid high between bytes
	LED_DATA_PORT.OUTSET = LED_DATA_PIN;
	LED_DATA_PORT.DIRSET = LED_DATA_PIN;

#ifdef VISIBLE_SCK_PIN
	// Set SCK low
	LED_SCK_PORT.OUTCLR = LED_SCK_PIN;
	LED_SCK_PORT.DIRSET = LED_SCK_PIN;
#endif
}


void LED_Write_LED_RGB(uint24_t LED)
{
	for(uint8_t Bit_Count = LED_BIT_SIZE_RGB; Bit_Count > 0; Bit_Count--) {
		if(LED & LED_MASK_RGB) {     // Send "1"
			LED_Send_Bit(LED_BIT_ONE);
		} else {    // Send "0"
			LED_Send_Bit(LED_BIT_ZERO);
		}
		LED <<= 1;
	}
}

void LED_Write_LED_RGB_String(uint24_t *LED_String, uint16_t LED_string_length)
{
	while(LED_string_length--) {
		LED_Write_LED_RGB(*LED_String++);
	}

#if (F_CPU == 20000000ul)
	LED_USART.STATUS = USART_TXCIF_bm;
	while(!(LED_USART.STATUS & USART_TXCIF_bm)) {
		// Wait for last byte sent
	}
#endif
}

void LED_Fill_LED_RGB_String(uint24_t Color, uint16_t LED_string_length)
{
	while(LED_string_length--) {
		LED_Write_LED_RGB(Color);
	}

#if (F_CPU == 20000000ul)
	LED_USART.STATUS = USART_TXCIF_bm;
	while(!(LED_USART.STATUS & USART_TXCIF_bm)) {
		// Wait for last byte sent
	}
#endif
}

void LED_Write_LED_RGBW(uint32_t LED)
{
	for(uint8_t Bit_Count = LED_BIT_SIZE_RGBW; Bit_Count > 0; Bit_Count--) {
		if(LED & LED_MASK_RGBW) {     // Send "1"
			LED_Send_Bit(LED_BIT_ONE);
		} else {    // Send "0"
			LED_Send_Bit(LED_BIT_ZERO);
		}
		LED <<= 1;
	}
}

void LED_Write_LED_RGBW_String(uint32_t *LED_String, uint16_t LED_string_length)
{
	while(LED_string_length--) {
		LED_Write_LED_RGBW(*LED_String++);
	}

#if (F_CPU == 20000000ul)
	LED_USART.STATUS = USART_TXCIF_bm;
	while(!(LED_USART.STATUS & USART_TXCIF_bm)) {
		// Wait for last byte sent
	}
#endif
}

void LED_Fill_LED_RGBW_String(uint32_t Color, uint16_t LED_string_length)
{
	while(LED_string_length--) {
		LED_Write_LED_RGBW(Color);
	}

#if (F_CPU == 20000000ul)
	LED_USART.STATUS = USART_TXCIF_bm;
	while(!(LED_USART.STATUS & USART_TXCIF_bm)) {
		// Wait for last byte sent
	}
#endif
}

/*
 * Converts HSB (Hue, Saturation, Brightness) values
 * to their corresponding 8-bit RGB values and returns
 * the results as a color_t
 *
 * Legal ranges:
 * h - 0..1535
 * s - 0..255
 * b - 0..255
 *
 * NOTE:
 * Optimized for speed
 * No rounding
 * b is saturated at 254 to avoid
 */
color_t hsb2rgb(hsb_t led_array)
{
	uint16_t h = led_array.h;
	uint8_t s = led_array.s;
	uint8_t b = led_array.b;

	uint8_t c = (b * s) / 256;
	uint8_t m = b >= c ? b - c : 0;
	uint8_t x = (h % 256) * c / 256;
	uint8_t sector = h >> 8;

	if (sector == 0) return (color_t) { .r=c+m   , .g=x+m   , .b=0+m   };
	if (sector == 1) return (color_t) { .r=c-x+m , .g=c+m   , .b=0+m   };
	if (sector == 2) return (color_t) { .r=0+m   , .g=c+m   , .b=x+m   };
	if (sector == 3) return (color_t) { .r=0+m   , .g=c-x+m , .b=c+m   };
	if (sector == 4) return (color_t) { .r=x+m   , .g=0+m   , .b=c+m   };
	if (sector == 5) return (color_t) { .r=c+m   , .g=0+m   , .b=c-x+m };
	return (color_t) { .r=0     , .g=0     , .b=0     };
}
